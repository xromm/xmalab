XMALab is software for marker-based fluoromicrometry and XROMM, with tools for standard (non-X-ray) video analysis as well. The developer is Dr. Ben Knorlein, a computer-vision expert and software engineer at the Center for Computation and Visualization at Brown University. XMALab has been described and validated in [a peer-reviewed paper ](http://jeb.biologists.org/content/early/2016/09/21/jeb.145383) in the *Journal of Experimental Biology*. Publications using XMALab must provide attribution by citing: Kn�rlein, B.J., Baier, D.B., Gatesy, S.M., Laurence-Chasen, J.D. and Brainerd, E.L. 2016. Validation of XMALab software for marker-based XROMM. Journal of Experimental Biology, 219: 3701-3711. doi:10.1242/jeb.145383.

XMALab integrates distortion correction, calibration, marker tracking, rigid body calculations and filtering all into one program. XMALab replaces all
components of the MATLAB X-ray Project workflow. For marker-based XROMM, XMALab generates animation matrices suitable for animating bones in Autodesk Maya. XMALab also includes tools, such as checkerboard calibration, for motion analysis with standard (non-Xray) video.
## Download the latest binary release #
### Version 2.1.0 : [Mac](https://bitbucket.org/xromm/xmalab/downloads/XMALab_2.1.0.dmg) and [Windows](https://bitbucket.org/xromm/xmalab/downloads/XMALab_Setup-2.1.0.msi)
### Visit the [XMALab Bitbucket Wiki](https://bitbucket.org/xromm/xmalab/wiki/Home) for User Manual and Version History
### Join the [XMALab Google Group](https://groups.google.com/a/brown.edu/forum/?hl=en#!forum/xmalab) to be notified of future releases and pose questions to the group.
### XMALab Tutorial with example data on [xmaportal.org/sandbox/](http://xmaportal.org/sandbox/larequest.php?request=explorePublicStudy&StudyID=49&instit=SANDBOX1)
### Instructions on how to build the source code can be found [here](https://bitbucket.org/xromm/xmalab/wiki/Instructions%20for%20developers)

XMALab development is supported by the US National Science Foundation through an Advances in Biological Informatics grant to PI Elizabeth Brainerd and 
CoPIs Stephen Gatesy and David Baier.